package bazarbazaraps.worldcupgoal

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import bazarbazaraps.worldcupgoal.R

class MySimpleArrayAdapter(context: Context?, private val values: Array<String>) : ArrayAdapter<String>(context, -1, values) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val rowView = inflater.inflate(R.layout.row_main, parent, false)
        val textView = rowView.findViewById(R.id.sampletextView) as TextView

        textView.text = values[position]
        // change the icon for Windows and iPhone


        return rowView
    }
}
