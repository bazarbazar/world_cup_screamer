package bazarbazaraps.worldcupgoal


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import bazarbazaraps.worldcupgoal.R
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class ThirtheenthFragment : Fragment() {
    private lateinit var mInterstitialAd: InterstitialAd
    private lateinit var listView: ListView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        MobileAds.initialize(context, "ca-app-pub-9103822257004637~2938245107")

        mInterstitialAd = InterstitialAd(context)
        mInterstitialAd.adUnitId = "ca-app-pub-9103822257004637/9646660666"
        mInterstitialAd.loadAd(AdRequest.Builder().build())

        val testList = arrayOf(
                "Spain Goal 1",
                "Spain Goal 2",
                "Spain Goal 3",
                "Spain Goal 4",
                "Spain Goal 5",
                "Spain Goal 6",
                "Spain Goal 7",
                "Spain Goal 8",
                "Spain Goal 9",
                "Spain Goal 10",
                "Spain Goal 11",
                "Spain Goal 12",
                "Spain Goal 13",
                "Spain Goal 14",
                "Spain Goal 15",
                "Spain Goal 16",
                "Spain Tiger Goal")

        val sample_names_const = arrayOf(
                "spaingoal1",
                "spaingoal2",
                "spaingoal3",
                "spaingoal4",
                "spaingoal5",
                "spaingoal6",
                "spaingoal7",
                "spaingoal8",
                "spaingoal9",
                "spaingoal10",
                "spaingoal11",
                "spaingoal12",
                "spaingoal13",
                "spaingoal14",
                "spaingoal15",
                "spaingoal16",
                "spaintigergoal")

        val MP_instances = ArrayList<Int>()

        MP_instances.add(R.raw.spaingoal1)
        MP_instances.add(R.raw.spaingoal2)
        MP_instances.add(R.raw.spaingoal3)
        MP_instances.add(R.raw.spaingoal4)
        MP_instances.add(R.raw.spaingoal5)
        MP_instances.add(R.raw.spaingoal6)
        MP_instances.add(R.raw.spaingoal7)
        MP_instances.add(R.raw.spaingoal8)
        MP_instances.add(R.raw.spaingoal9)
        MP_instances.add(R.raw.spaingoal10)
        MP_instances.add(R.raw.spaingoal11)
        MP_instances.add(R.raw.spaingoal12)
        MP_instances.add(R.raw.spaingoal13)
        MP_instances.add(R.raw.spaingoal14)
        MP_instances.add(R.raw.spaingoal15)
        MP_instances.add(R.raw.spaingoal16)
        MP_instances.add(R.raw.spaintigergoal)



        val rootView = inflater.inflate(R.layout.fragment_thirtheenth, container, false)


        val adapter = MySimpleArrayAdapter(context, testList)

        listView = rootView.findViewById(R.id.listview13)
        listView.adapter = adapter

        val mediaPlayer = MediaPlayerSingleton.instance

        //mediaPlayer.mp = MediaPlayer.create(context, R.raw.argentinagoal1)


        fun share_audio(file_path: String) {
            val audio = Uri.parse(file_path)

            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "audio/mpeg"
            shareIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            shareIntent.putExtra(Intent.EXTRA_STREAM, audio);

            startActivity(Intent.createChooser(shareIntent, "Goal share..."))

        }

        listView.setOnItemClickListener {

            parent, view, position, id ->
            mediaPlayer.reset_sound(MP_instances[position], context)
            // reset_sound(mediaPlayer, MP_instances[position])
            mediaPlayer.MPplay(MP_instances[position], context)
            //  mInterstitialAd.show()
            mInterstitialAd.show()
        }

        listView.setOnItemLongClickListener { parent, view, position, id ->
            share_audio("android.resource://bazarbazaraps.worldcupgoal/raw/" + sample_names_const[position])
            true
        }



        return rootView


    }
}