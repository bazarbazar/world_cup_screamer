package bazarbazaraps.worldcupgoal


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import bazarbazaraps.worldcupgoal.R


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class TenthFragment : Fragment() {

    private lateinit var listView: ListView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        val testList = arrayOf(
                "Italy Goal 1",
                "Italy Goal 2",
                "Italy Goal 3",
                "Italy Goal 4",
                "Italy Goal 5",
                "Italy Goal 6",
                "Italy Goal 7",
                "Italy Goal 8",
                "Italy Goal 9",
                "Italy Goal 10",
                "Italy Goal 11",
                "Italy Goal 12",
                "Italy Goal 13",
                "Italy Goal 14",
                "Italy Goal 15",
                "Italy Goal 16")

        val sample_names_const = arrayOf(
                "italygoal1",
                "italygoal2",
                "italygoal3",
                "italygoal4",
                "italygoal5",
                "italygoal6",
                "italygoal7",
                "italygoal8",
                "italygoal9",
                "italygoal10",
                "italygoal11",
                "italygoal12",
                "italygoal13",
                "italygoal14",
                "italygoal15",
                "italygoal16")

        val MP_instances = ArrayList<Int>()

        MP_instances.add(R.raw.italygoal1)
        MP_instances.add(R.raw.italygoal2)
        MP_instances.add(R.raw.italygoal3)
        MP_instances.add(R.raw.italygoal4)
        MP_instances.add(R.raw.italygoal5)
        MP_instances.add(R.raw.italygoal6)
        MP_instances.add(R.raw.italygoal7)
        MP_instances.add(R.raw.italygoal8)
        MP_instances.add(R.raw.italygoal9)
        MP_instances.add(R.raw.italygoal10)
        MP_instances.add(R.raw.italygoal11)
        MP_instances.add(R.raw.italygoal12)
        MP_instances.add(R.raw.italygoal13)
        MP_instances.add(R.raw.italygoal14)
        MP_instances.add(R.raw.italygoal15)
        MP_instances.add(R.raw.italygoal16)



        val rootView = inflater.inflate(R.layout.fragment_tenth, container, false)


        val adapter = MySimpleArrayAdapter(context, testList)

        listView = rootView.findViewById(R.id.listview10)
        listView.adapter = adapter

        val mediaPlayer = MediaPlayerSingleton.instance

        //mediaPlayer.mp = MediaPlayer.create(context, R.raw.argentinagoal1)


        fun share_audio(file_path: String) {
            val audio = Uri.parse(file_path)

            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "audio/mpeg"
            shareIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            shareIntent.putExtra(Intent.EXTRA_STREAM, audio);

            startActivity(Intent.createChooser(shareIntent, "Goal share..."))

        }

        listView.setOnItemClickListener {

            parent, view, position, id ->
            mediaPlayer.reset_sound(MP_instances[position], context)
            // reset_sound(mediaPlayer, MP_instances[position])
            mediaPlayer.MPplay(MP_instances[position], context)
            //  mInterstitialAd.show()

        }

        listView.setOnItemLongClickListener { parent, view, position, id ->
            share_audio("android.resource://bazarbazaraps.worldcupgoal/raw/" + sample_names_const[position])
            true
        }



        return rootView


    }


}
