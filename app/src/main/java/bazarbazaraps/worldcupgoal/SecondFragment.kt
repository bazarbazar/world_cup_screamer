package bazarbazaraps.worldcupgoal


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.widget.Toast
import bazarbazaraps.worldcupgoal.R
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class SecondFragment : Fragment() {
    private lateinit var mInterstitialAd: InterstitialAd
    private lateinit var listView: ListView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        MobileAds.initialize(context, "ca-app-pub-9103822257004637~2938245107")

        mInterstitialAd = InterstitialAd(context)
        mInterstitialAd.adUnitId = "ca-app-pub-9103822257004637/9646660666"
        mInterstitialAd.loadAd(AdRequest.Builder().build())


        // Inflate the layout for this fragment



        val testList = arrayOf(
                "Brazil Goal 1",
                "Brazil Goal 2",
                "Brazil Goal 3",
                "Brazil Goal 4",
                "Brazil Goal 5",
                "Brazil Goal 6",
                "Brazil Goal 7",
                "Brazil Goal 8",
                "Brazil Goal 9",
                "Brazil Goal 10")

        val sample_names_const = arrayOf(
                "brazilgoal1",
                "brazilgoal2",
                "brazilgoal3",
                "brazilgoal4",
                "brazilgoal5",
                "brazilgoal6",
                "brazilgoal7",
                "brazilgoal8",
                "brazilgoal9",
                "brazilgoal10")

        val  MP_instances = ArrayList<Int>()

        MP_instances.add(R.raw.brazilgoal1)
        MP_instances.add(R.raw.brazilgoal2)
        MP_instances.add(R.raw.brazilgoal3)
        MP_instances.add(R.raw.brazilgoal4)
        MP_instances.add(R.raw.brazilgoal5)
        MP_instances.add(R.raw.brazilgoal6)
        MP_instances.add(R.raw.brazilgoal7)
        MP_instances.add(R.raw.brazilgoal8)
        MP_instances.add(R.raw.brazilgoal9)
        MP_instances.add(R.raw.brazilgoal10)

        val rootView = inflater.inflate(R.layout.fragment_second, container, false)



        val adapter = MySimpleArrayAdapter(context, testList)

        listView = rootView.findViewById(R.id.listview2)
        listView.adapter = adapter

        val mediaPlayer = MediaPlayerSingleton.instance

        //mediaPlayer.mp = MediaPlayer.create(context, R.raw.argentinagoal1)



        fun share_audio( file_path: String ){
            val audio =  Uri.parse(file_path)

            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "audio/mpeg"
            shareIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            shareIntent.putExtra(Intent.EXTRA_STREAM, audio);

            startActivity(Intent.createChooser(shareIntent, "Goal share..."))

        }

        listView.setOnItemClickListener {

            parent, view, position, id ->
            mediaPlayer.reset_sound(MP_instances[position],context)
            // reset_sound(mediaPlayer, MP_instances[position])
            mediaPlayer.MPplay( MP_instances[position],context)
            //  mInterstitialAd.show()
            mInterstitialAd.show()
        }

        listView.setOnItemLongClickListener { parent, view, position, id ->
            share_audio("android.resource://bazarbazaraps.worldcupgoal/raw/" + sample_names_const[position])
            true
        }

        return rootView

    }
}

